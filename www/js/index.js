/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var util = {
    latitude: undefined,
    longitude: undefined,
	gpsWasDisabled: "0",

    getDeviceTime: function() {
        now = new Date();
        return now.getTime();
    },
    savePosition: function(position) {
        util.latitude = position.coords.latitude;
        util.longitude = position.coords.longitude;
        /*alert('Latitude: '           + position.coords.latitude +
              'Longitude: '          + position.coords.longitude +
              'Altitude: '           + position.coords.altitude +
              'Accuracy: '           + position.coords.accuracy +
              'Altitude Accuracy: '  + position.coords.altitudeAccuracy +
              'Heading: '            + position.coords.heading +
              'Speed: '              + position.coords.speed +
              'Timestamp: '          + position.timestamp);*/
        console.log("latitude : " + util.latitude + "\n" + "longitude : " + util.longitude);
    },
    getLatitude: function() {
        if (util.gpsWasDisabled == "0") {
            return util.latitude;
            //return "40.908235";
        } else {
        	return "0";
        }
    },
    getLongitude: function() {
        if (util.gpsWasDisabled == "0") {
            return util.longitude;
            //return "-73.850219";
        } else {
			return "0";
        }
    },
    getPreference: function(string) {
    	function ok(value) {
    		return value;
    	}
		function fail(error) {
			return error;
		}
		var prefs = plugins.appPreferences;
		prefs.fetch(ok, fail, string);
    },
    savePreference: function(key, value) {
    	function ok(value) {
    		return value;
    	}
		function fail(error) {
			return;
		}
		var prefs = plugins.appPreferences;
		prefs.store(ok, fail, key, value);
    },
	takePhoto: function() {
		function onSuccess(imageData) {
			var image = document.getElementById('sendImage');
			image.src = "data:image/jpeg;base64," + imageData;
			app.imageData=imageData;
			//alert(imageData.length);
		}

		function onFail(message) {
			//alert('Failed because: ' + message);
		}
		navigator.camera.getPicture(onSuccess, onFail, 
			{quality:50, 
			destinationType:Camera.DestinationType.DATA_URL,
			sourceType: app.sourceType,
			allowEdit : true});
	},
	validateUrl:function(value) {
		var message;
		var myRegExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;

		if (!myRegExp.test(value)){
			//	message = "Not a valid URL.";
			return false;
		}else{
			//message =  "Its a valid URL."
			return true;
		}
	},
	validatePage:function(value) {
		var message;
		var myRegExp =/^[a-zA-Z0-9\,\-\+\_]+$/i;
		if (myRegExp.test(value))
		{
			return true;
		}  else {
			return false;
		}
	}
};

var constant = {
    baseUrl: "https://www.my-pwp.net/",

    loginUrl: "Account/LoginMobile",
    logoutUrl: "Account/LogoutMobile",
	sendPhotoUrl:"Home/ReceivePhoto",

    dayOfWeek: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
    months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
};

var serverApi = {
    doLogIn: function(username, password) {
        $("#waiting").show();
        var params = {
            UserName: username,
            Password: password
        };
		//alert(JSON.stringify(params));
		$.support.cors = true;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(params),
            contentType: "application/json",
            dataType: 'json',
			isLocal:true,
            url: constant.baseUrl + constant.loginUrl,
            success: function(data) {
				//alert(JSON.stringify(data));
				if (data.StatusCode==200 && data.Message=="Login completed") {
					$("#login-err").hide();
					app.username = username;
					app.password = password;
					app.showMainPage();
				} else {
					$("#login-err").html("<span>Login Failed!<br>Please input correct username or password!</span>");
					$("#login-err").show();
				}				
                $("#waiting").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#login-err").show();
                $("#waiting").hide();
            }
        });
    },

	doLogOut: function() {
		$("#waiting").show();
        var params = {
			username: app.username
        };
        //alert(JSON.stringify(params));
		$.support.cors = true;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(params),
            contentType: "application/json",
            dataType: 'json',
            url: constant.baseUrl + constant.logoutUrl,
            success: function(data) {
                app.showLoginPage();
                $("#waiting").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#waiting").hide();
            }
        });

    },
    doSendPhoto: function(pTitle, pageUrl, descript, type) {
        $("#waiting").show();
        var params = {
			Title: pTitle,
			PageURL: pageUrl,
			Description: descript,
			PostType: type,
			Latitude: util.getLatitude(),
            Longitude: util.getLongitude(),
			Photo: app.imageData
        };
        //alert(JSON.stringify(params));
		$.support.cors = true;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(params),
            contentType: "application/json",
            dataType: 'json',
			isLocal:true,
            url: constant.baseUrl + constant.sendPhotoUrl,
            success: function(data) {
				//alert(JSON.stringify(data));
				if (data.StatusCode==200 && data.Message==null) {
					$("#send-err").hide();
					$("#send-success").show();
				} else {
					$("#send-success").hide();
					$("#send-err").text(data.Message);
					$("#send-err").show();
				}				
                $("#waiting").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
				//alert(JSON.stringify(data));
                $("#waiting").hide();
            }
        });
    }
};

var app = {
    // Application Constructor
    currentPage: '',

    loginBtn: undefined,
    sendPhotoBtn: undefined,
    logoutBtn: undefined,

    username: "",
	password: "",
	imageData: undefined,
	geoLocationOpt : {enableHighAccuracy: true, timeout: 5000},
	sourceType: "",

    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    showLoginPage: function() {
        app.onChangePage();
        $(".content").load("login.html", function() {
            app.currentPage = "LoginPage";
            app.onLoginLoad();
        });
    },
    showMainPage: function() {
        app.onChangePage();
        $(".content").load("mainview.html", function() {
            app.currentPage = "MainPage";
            app.onMainLoad();
        });
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        if (window.device.platform === 'iOS'/* && parseFloat(window.device.version) === 7.0*/) {
            StatusBar.overlaysWebView(false);
            StatusBar.backgroundColorByName("black");
            navigator.splashscreen.hide();
        }
//		document.addEventListener('keydown', this.onKeyEvent, false);

        app.showLoginPage();
    },

    onLoginLoad: function() {
		// top bar
		$(".top-bar").hide();
        // Bind
        var prefs = plugins.appPreferences;
        
        // get username from preference
    	function successGetUsername(value) {
   			$("#username").val(value);
    	}
		function successGetPassword(value) {
   			$("#password").val(value);
    	}
		function fail(error) {}		
		// get saveoption from preference
		prefs.fetch(successGetUsername, fail, "username");
		prefs.fetch(successGetPassword, fail, "password");
        
        $("#username").bind('keydown', app.onKeyEvent);
        $("#password").bind('keydown', app.onKeyEvent);
        
        app.loginBtn = $("#login-btn");
        app.loginBtn.bind('click', app.onClickLogin);
    },
    onLoginUnload: function() {
    	$("#username").unbind();
        $("#password").unbind();
        app.loginBtn.unbind();
    },

    onMainLoad: function() {
		$(".top-bar").show();
		$("#user-name-text").text(app.username);
		$("#description").text("");

        // Bind
		$("#phototypeGroup").hide();
		$("#photo").bind('click', app.takePhoto);
		$("#btn-library").bind('click', app.setTypeLibrary);
		$("#btn-camera").bind('click', app.setTypeCamera);
		$("#btn-cancel").bind('click', app.cancelPhoto);

		$("#photoTitle").bind('keydown', app.onKeyEvent);
		$("#pageUrl").bind('keydown', app.onKeyEvent);
        $("#description").bind('keydown', app.onKeyEvent);

        app.sendPhotoBtn = $("#send-btn");
        app.sendPhotoBtn.bind('click', app.doSendPhoto);

        app.logoutBtn = $("#buttonLogout");
        app.logoutBtn.bind('click', app.onClickLogout);
    },
    onMainUnload: function() {
		$("#photo").unbind();
		$("#btn-library").unbind();
		$("#btn-camera").unbind();
		$("#btn-cancel").unbind();

		$("#photoTitle").unbind();
        $("#description").unbind();

        app.sendPhotoBtn.unbind();
        app.logoutBtn.unbind();
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onKeyEvent: function(event) {

		if (app.currentPage === "LoginPage") {
            if (event.keyCode == 13) {
    			app.onClickLogin();
				return;
			}
			if ($("#username").val()!="")
				$("#username").removeClass("required");
			if ($("#password").val()!="")
				$("#password").removeClass("required");
        } else if (app.currentPage === "MainPage") {
			if ($("#photoTitle").val()!="")
				$("#photoTitle").removeClass("required");
			if ($("#pageUrl").val()!="")
				$("#pageUrl").removeClass("required");
			if ($("#description").val()!="")
				$("#description").removeClass("required");
        }	
    },
	takePhoto: function() {
		$("#phototypeGroup").show();
    },
	setTypeLibrary: function() {
		app.sourceType = navigator.camera.PictureSourceType.PHOTOLIBRARY;
		$("#phototypeGroup").hide();
		util.takePhoto();
    },
	setTypeCamera: function() {
		app.sourceType = navigator.camera.PictureSourceType.CAMERA;
		$("#phototypeGroup").hide();
		util.takePhoto();
    },
	cancelPhoto: function() {
		$("#phototypeGroup").hide();
    },
    onClickLogin: function() {
        // save user name and option
        var username = $("#username").val();
        var password = $("#password").val();
		var errMsg = "";
        if (username === "") {
			errMsg = "Please input your username";
			$("#username").addClass("required");
		}
        if (password === "") {
			if (errMsg=="")
				errMsg = "Please input your password";
			else 
				errMsg = errMsg + "<br>Please input your password";
			$("#password").addClass("required");
        }
		if (errMsg != "")
		{
			errMsg = "<span>" + errMsg + "</span>";
			$("#login-err").html(errMsg);
        	$("#login-err").show();
            return;
		}
        
        function ok(value) {
        	//alert("save success"+value);
        }
		function fail(error) {
			//alert("save fail"+error);
		}
		var prefs = plugins.appPreferences;
		prefs.store(ok, fail, "username", username);
		prefs.store(ok, fail, "password", password);
		
		$("#login-err").hide();
        serverApi.doLogIn(username, password);
    },
    onClickLogout: function() {
        //serverApi.doLogOut();
		app.showLoginPage();
    },
    doSendPhoto: function() {
		$("#send-success").hide();

		var pTitle = $("#photoTitle").val();
		var pageUrl = $("#pageUrl").val();
		var descript = $("#description").val();
		var pType = $("input[name=pType]:checked").val();
		var errMsg = "";

		if (app.imageData == undefined) {
			errMsg = "Please select photo.";
		}
		if (pTitle == "") {
			if (errMsg=="")
				errMsg = "Please type photo title.";
			else 
				errMsg = errMsg + "<br>Please type photo title.";
			
			$("#photoTitle").addClass("required");
		}
		if (pageUrl == "") {
			if (errMsg=="")
				errMsg = "Please type page URL.";
			else
				errMsg = errMsg + "<br>Please type page URL.";
			
			$("#pageUrl").addClass("required");
		} else if (!util.validatePage(pageUrl)) {
			if (errMsg=="")
				errMsg = "Please type valid page URL.";
			else
				errMsg = errMsg + "<br>Please type valid page URL.";
			
			$("#pageUrl").addClass("required");
		} else {
			$("#pageUrl").removeClass("required");
		}

		if (descript == "") {
			if (errMsg=="")
				errMsg = "Please type photo description.";
			else 
				errMsg = errMsg + "<br>Please type photo description.";

			$("#description").addClass("required");
		}

		if (errMsg != "")
		{
			errMsg = "<span>" + errMsg + "</span>";
			$("#send-err").html(errMsg);
        	$("#send-err").show();
            return;
		}

		$("#send-err").hide();
		
//		alert("title : "+pTitle+",  description : "+descript + ",  type:"+pType);
		var onSuccess = function(position) {
            util.savePosition(position);
            util.gpsWasDisabled = "0";
            serverApi.doSendPhoto(pTitle, pageUrl, descript, pType);
        };

        function onError(error) {
            //alert(error.message);
            //alert("Could not get location. \nPlease Confirm GPS Setting.")
            //window.plugins.toast.showShortBottom("GPS setting was disabled");
            util.gpsWasDisabled = "1";
            serverApi.doSendPhoto(pTitle, pageUrl, descript, pType);
        }
        
		$("#waiting").show();
        navigator.geolocation.getCurrentPosition(onSuccess, onError, app.geoLocationOpt);
    },
    onChangePage: function() {
        if (app.currentPage === "LoginPage") {
            app.onLoginUnload();
        } else if (app.currentPage === "MainPage") {
            app.onMainUnload();
        }
    }
};


app.initialize();